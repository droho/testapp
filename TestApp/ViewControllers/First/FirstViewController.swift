//
//  FirstViewController.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    // MARK: - Views

    var bottomDrawer: BottomDrawer?
    
    // MARK: - Private properties
    
    private var secondController: UIViewController?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    // MARK: - Configuration functions
    
    private func configureView() {
        createSecondController()
        createBottomDrawer()
        setTitle()
    }
    
    private func createSecondController() {
        secondController = SecondViewController.controllerFromStoryboard(.main)
    }
    
    private func createBottomDrawer() {
        guard let controller = secondController else { return }
        let bottomDrawerFrame = CGRect(x: view.frame.minX, y: view.frame.maxY - 100, width: view.frame.width, height: view.frame.height)
        bottomDrawer = BottomDrawerView(frame: bottomDrawerFrame)
        bottomDrawer?.present(in: self)
        bottomDrawer?.addContent(controller)
    }
    
    private func setTitle() {
        title = Localizable.NavigationBarTitles.lava.text
    }
    
}

// MARK: - BottomDrawerDelegate

extension FirstViewController: BottomDrawerDelegate {
    
    func bottomDrawerDidBeginMove(_ sender: BottomDrawer, direction: PanDirection) {

    }
    
    func bottomDrawerDidFinishMove(_ sender: BottomDrawer) {
        
    }

}
