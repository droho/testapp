//
//  SecondViewController.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
        }
    }
    
    // MARK: - Public properties
    
    var presenter: SecondViewPresenter?
    
    // MARK: - Private properties
    
    private var cells: [TableViewItem] = []
    private var texts: [String] = []
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    // MARK: - Configuration functions
    
    private func configureView() {
        presenter = SecondPresenter(view: self)
        configureCells()
        registerCells()
        presenter?.getData()
    }
    
    private func configureCells() {
        let headerItem = HeaderItem(cellText: Localizable.NavigationBarTitles.lava.text)
        cells.append(headerItem)
        
        let collectionItem = CollectionItem()
        cells.append(collectionItem)
        
        let textItem = TextItem(texts: texts)
        cells.append(textItem)
    }
    
    private func registerCells() {
        cells.forEach {
            let nib = UINib(nibName: $0.cellName, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: $0.cellId)
        }
    }
    
}

// MARK: - SecondView

extension SecondViewController: SecondView {
    
    func reload(with data: [String]) {
        guard
            let item = cells.first(where: { $0 is TextItem }),
            let textItem = item as? TextItem
            else { return }
        
        textItem.texts = data
        tableView.reloadData()
    }
    
}

// MARK: - UITableViewDataSource

extension SecondViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellItem = cells[indexPath.section]
        
        switch cellItem.type {
        case .header:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellItem.cellId, for: indexPath) as? HeaderTableViewCell {
                cell.titleLabel.text = (cellItem as? HeaderItem)?.cellText
                return cell
            }
        case .collection:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellItem.cellId, for: indexPath) as? CollectionTableViewCell {
                return cell
            }
        case .text:
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellItem.cellId, for: indexPath) as? TextTableViewCell {
                cell.titleLabel.text = (cellItem as? TextItem)?.texts[indexPath.row]
                return cell
            }
        }
        
        return UITableViewCell()
    }

}
