//
//  SecondTemplate.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

protocol SecondViewPresenter {
    
    init(view: SecondView)
    var data: [String] { get set }
    func getData()
    
}

protocol SecondView: class {
    
    func reload(with data: [String])
    
}
