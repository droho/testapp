//
//  SecondPresenter.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class SecondPresenter: SecondViewPresenter {
    
    unowned var view: SecondView
    var data: [String] = []
    
    required init(view: SecondView) {
        self.view = view
    }
    
    func getData() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            guard let strongSelf = self else { return }
            for index in 0...50 {
                strongSelf.data.append("-----\(index)-----")
            }
            strongSelf.view.reload(with: strongSelf.data)
        }
    }
    
}
