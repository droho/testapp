//
//  CustomCollectionView.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class CustomCollectionView: UICollectionView {
    
    // MARK: - Public properties
    
    var numberOfItems: Int? {
        didSet {
            addPageControlToSuperview()
        }
    }
    
    // MARK: - Private properties
    
    private var pageControl: UIPageControl?
    private let pageControlHeight: CGFloat = 30
    
    // MARK: - Inits
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    // MARK: - Configuration functions
    
    private func configureView() {
        updateCellsLayout()
        collectionViewLayout = collectionLayout()
        delegate = self
    }
    
    private func collectionLayout() -> CollectionLayout {
        let layout = CollectionLayout()
        layout.scrollDirection = .horizontal
        
        return layout
    }
    
    private func addPageControlToSuperview() {
        guard
            let superview = superview,
            let numberOfItems = numberOfItems
            else { return }
        
        pageControl = UIPageControl(frame: CGRect(x: frame.minX, y: frame.maxY - pageControlHeight, width: frame.width, height: pageControlHeight))
        guard let pageControl = pageControl else { return }
        pageControl.numberOfPages = numberOfItems
        superview.addSubview(pageControl)
    }
    
    private func updateCellsLayout()  {
        let centerX = self.contentOffset.x + (self.frame.size.width) / 2
        
        for cell in self.visibleCells {
            var offsetX = centerX - cell.center.x
            if offsetX < 0 {
                offsetX *= -1
            }
            cell.transform = CGAffineTransform.identity
            let offsetPercentage = offsetX / (self.bounds.width * 2)
            let scaleX = 1 - offsetPercentage
            cell.contentView.subviews.last?.alpha = 1 - scaleX
            cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)
        }
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension CustomCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize: CGSize = collectionView.bounds.size
        return cellSize
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCellsLayout()
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}
