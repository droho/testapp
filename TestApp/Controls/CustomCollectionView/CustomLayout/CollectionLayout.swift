//
//  CollectionLayout.swift
//  TestApp
//
//  Created by Damian Drohobycki on 24/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

public class CollectionLayout: UICollectionViewFlowLayout {
    
    // MARK: - Public properties
    
    var insertTopCells: Bool = false
    var topInsertionsSize: CGSize = CGSize.zero
    var lastPoint: CGPoint = CGPoint.zero
    
    // MARK: - Override
    
    override public func prepare() {
        super.prepare()
        guard let collectionView = collectionView else { return }
        let oldSize: CGSize = topInsertionsSize
        
        if insertTopCells {
            let newSize: CGSize  = collectionViewContentSize
            let xOffset: CGFloat = collectionView.contentOffset.x + (newSize.width - oldSize.width)
            let newOffset: CGPoint = CGPoint(x: xOffset, y: collectionView.contentOffset.y)
            collectionView.contentOffset = newOffset
        } else {
            insertTopCells = false
        }
        
        topInsertionsSize = collectionViewContentSize
    }
    
    override public func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
         guard
            let collectionView = collectionView,
            let layoutAttributesElements = layoutAttributesForElements(in: collectionView.bounds)
            else { return CGPoint(x: 0, y: 0) }
        
        var layoutAttributes: Array = layoutAttributesElements
        
        if layoutAttributes.count == 0 {
            return proposedContentOffset
        }
        
        var targetIndex = layoutAttributes.count / 2
        
        let point = velocity.x
        
        switch point {
        case _ where point > 0:
             targetIndex += 1
        case _ where point < 0:
             targetIndex -= 1
        case 0:
            return lastPoint
        default:
            break
        }
        
        if targetIndex >= layoutAttributes.count {
            targetIndex = layoutAttributes.count - 1
        }
        
        if targetIndex < 0 {
            targetIndex = 0
        }
        
        let targetAttribute = layoutAttributes[targetIndex]
        
        lastPoint = CGPoint(x: targetAttribute.center.x - collectionView.bounds.size.width * 0.5, y: proposedContentOffset.y)
        return lastPoint
    }
    
}
