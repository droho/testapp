//
//  XibSetup.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

import UIKit

protocol XibSetup: class {
    
    var view: UIView! { get set }
    func xibSetup()
    
}

extension XibSetup where Self: UIView {
    
    func xibSetup() {
        view = self.loadNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(view)
    }
    
}
