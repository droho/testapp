//
//  BottomDrawerView.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class BottomDrawerView: UIView, XibSetup {
    
    // MARK: - Views
    
    var view: UIView!
    
    // MARK: - Outlets
    
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Public properties
    
    var delegate: BottomDrawerDelegate?
    
    // MARK: - Private properties
    
    private var animator: UIDynamicAnimator?
    private var snapBehavior: UISnapBehavior?
    private var itemBehavior: UIDynamicItemBehavior?
    private var viewStartYPosition: CGFloat?
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    // MARK: - Actions
    
    @objc func pan(_ recognizer: UIPanGestureRecognizer) {
        onPanGestureAction(recognizer: recognizer)
    }
    
    // MARK: - Configuration functions
    
    private func configureView() {
        xibSetup()
        configurePanGesture()
        viewStartYPosition = frame.midY
    }
    
    private func configurePanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan(_:)))
        topBar.addGestureRecognizer(panGesture)
    }
    
    private func configureAnimator() {
        guard let superview = superview else { return }
        animator = UIDynamicAnimator(referenceView: superview)
        animator?.delegate = self
        snapBehavior = UISnapBehavior(item: self, snapTo: superview.center)
        itemBehavior = UIDynamicItemBehavior(items: [self])
        itemBehavior?.allowsRotation = false
    }
    
    private func onPanGestureAction(recognizer: UIPanGestureRecognizer) {
        guard let superview = superview else { return }
        
        switch(recognizer.state) {
        case .began:
            onBeganPanGesture()
        case .changed:
            onChangedPanGesture(recognizer)
        case .ended:
            onEndedPanGesture(recognizer, superview: superview)
        default:
            break
        }
    }
    
    private func onChangedPanGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: view)
        self.center.y += translation.y
        recognizer.setTranslation(.zero, in: view)
    }
    
    private func onEndedPanGesture(_ recognizer: UIPanGestureRecognizer, superview: UIView) {
        guard
            let viewStartPosition = viewStartYPosition,
            let itemBehavior = itemBehavior,
            let snapBehavior = snapBehavior
            else { return }
        
        let velocity = recognizer.velocity(in: view).y
        if velocity > 0 {
            snapBehavior.snapPoint = CGPoint(x: superview.center.x, y: viewStartPosition)
            delegate?.bottomDrawerDidBeginMove(self, direction: .down)
        } else {
            snapBehavior.snapPoint = CGPoint(x: superview.center.x, y: superview.center.y)
            delegate?.bottomDrawerDidBeginMove(self, direction: .up)
        }
        
        animator?.addBehavior(itemBehavior)
        animator?.addBehavior(snapBehavior)
    }
    
    private func onBeganPanGesture() {
        animator?.removeAllBehaviors()
    }
    
}

// MARK: - BottomDrawer

extension BottomDrawerView: BottomDrawer {
    
    func present(in viewController: UIViewController) {
        delegate = viewController as? BottomDrawerDelegate
        viewController.view.addSubview(self)
        configureAnimator()
    }
    
    func addContent(_ contentViewController: UIViewController) {
        contentViewController.view.frame = containerView.bounds
        containerView.addSubview(contentViewController.view)
    }
    
}

// MARK: - UIDynamicAnimatorDelegate

extension BottomDrawerView: UIDynamicAnimatorDelegate {
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        delegate?.bottomDrawerDidFinishMove(self)
    }
    
}
