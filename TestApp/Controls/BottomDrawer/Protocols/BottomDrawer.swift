//
//  BottomDrawer.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

protocol BottomDrawerDelegate: class {
    
    func bottomDrawerDidBeginMove(_ sender: BottomDrawer, direction: PanDirection)
    func bottomDrawerDidFinishMove(_ sender: BottomDrawer)
    
}

protocol BottomDrawer {
    
    func present(in viewController: UIViewController)
    func addContent(_ contentViewController: UIViewController)
    
}
