//
//  StoryboardType.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

enum StoryboardType: String {
    
    case main = "Main"
    
}
