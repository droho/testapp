//
//  CollectionItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class CollectionItem: TableViewItem {
    
    var type: TableViewItemType {
        return .collection
    }
    
    var rowCount: Int {
        return 1
    }
    
    var cellName: String {
        return CollectionTableViewCell.className
    }
    
    var cellId: String {
        return "collectionCell"
    }
    
}
