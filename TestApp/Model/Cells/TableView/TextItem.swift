//
//  TextItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class TextItem: TableViewItem {
    
    var type: TableViewItemType {
        return .text
    }
    
    var cellName: String {
        return TextTableViewCell.className
    }
    
    var cellId: String {
        return "textCell"
    }
    
    var rowCount: Int {
        return texts.count
    }
    
    var texts: [String]
    
    init(texts: [String]) {
        self.texts = texts
    }
    
}
