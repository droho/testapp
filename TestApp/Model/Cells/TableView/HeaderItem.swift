//
//  HeaderItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class HeaderItem: TableViewItem {
    
    var type: TableViewItemType {
        return .header
    }
    
    var cellName: String {
        return HeaderTableViewCell.className
    }
    
    var cellId: String {
        return "headerCell"
    }
    
    var rowCount: Int {
        return 1
    }
    
    let cellText: String
    
    init(cellText: String) {
        self.cellText = cellText
    }
    
}
