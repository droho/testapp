//
//  ImageItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class ImageItem: CollectionViewItem {
    
    var type: CollectionViewItemType {
        return .image
    }
    
    var cellName: String {
        return ImageCollectionViewCell.className
    }
    
    var cellId: String {
        return "imageCell"
    }
    
    var rowCount: Int {
        return 1
    }
    
    let imageName: String
    
    init(imageName: String) {
        self.imageName = imageName
    }
    
}
