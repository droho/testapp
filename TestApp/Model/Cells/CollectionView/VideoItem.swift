//
//  VideoItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class VideoItem: CollectionViewItem {
    
    var type: CollectionViewItemType {
        return .video
    }
    
    var cellName: String {
        return VideoCollectionViewCell.className
    }
    
    var cellId: String {
        return "videoCell"
    }
    
    var rowCount: Int {
        return 1
    }
    
    let videoUrl: String?
    
    init(videoUrl: String?) {
        self.videoUrl = videoUrl
    }
    
}
