//
//  ArticleItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

class ArticleItem: CollectionViewItem {
    
    var type: CollectionViewItemType {
        return .article
    }
    
    var cellName: String {
        return ArticleCollectionViewCell.className
    }
    
    var cellId: String {
        return "articleCell"
    }
    
    var rowCount: Int {
        return 1
    }
    
}
