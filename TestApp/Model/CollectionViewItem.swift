//
//  CollectionViewItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

protocol CollectionViewItem {
    
    var type: CollectionViewItemType { get }
    var rowCount: Int { get }
    var cellName: String { get }
    var cellId: String { get }
    
}

enum CollectionViewItemType {
    
    case video
    case article
    case image
    
}
