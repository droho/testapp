//
//  PanDirection.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

enum PanDirection {
    
    case up
    case down
    
}
