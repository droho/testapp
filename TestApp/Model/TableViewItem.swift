//
//  TableViewItem.swift
//  TestApp
//
//  Created by Damian Drohobycki on 26/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

protocol TableViewItem {
    
    var type: TableViewItemType { get }
    var rowCount: Int { get }
    var cellName: String { get }
    var cellId: String { get }
    
}

enum TableViewItemType {
    
    case header
    case collection
    case text
    
}
