//
//  Localizable.swift
//  TestApp
//
//  Created by Damian Drohobycki on 25/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

protocol Localized { }

extension Localized where Self: RawRepresentable, Self.RawValue == String {
    
    var text: String {
        return NSLocalizedString(self.rawValue, value: "No localized string found", comment: "")
    }
    
}

enum Localizable {
    
    enum NavigationBarTitles: String, Localized {
        case lava
    }
    
}
