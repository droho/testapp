//
//  UICollectoinViewCell+Extensions.swift
//  TestApp
//
//  Created by Damian Drohobycki on 24/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    func addShadeView() {
        let blur = UIView()
        self.contentView.addSubview(blur)
        blur.backgroundColor = .white
        blur.alpha = 0
        blur.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            blur.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
            blur.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
            blur.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0),
            blur.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
            ])
    }
    
}
