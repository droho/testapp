//
//  CollectionTableViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit
class CollectionTableViewCell: UITableViewCell {

    // MARK: - Outelts
    
    @IBOutlet weak var collectionView: CustomCollectionView! {
        didSet {
            collectionView.dataSource = self
            configureCells()
            registerNibs()
            collectionView.numberOfItems = cells.count
        }
    }
    
    // MARK: - Private properties
    
    private var cells: [CollectionViewItem] = []
    private let videoStringUrl = "https://www.videvo.net/videvo_files/converted/2018_03/videos/180301_16_B_LunarYearsParade_20.mp412898_jw.mp4"
    private let imageName = "0"

    // MARK: - Configuration functions
    
    private func configureCells() {
        let videoItem = VideoItem(videoUrl: videoStringUrl)
        cells.append(videoItem)

        
        let articleItem = ArticleItem()
        cells.append(articleItem)
        
        let imageItem = ImageItem(imageName: imageName)
        cells.append(imageItem)
    }
    
    private func registerNibs() {
        cells.forEach {
            let nib = UINib(nibName: $0.cellName, bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: $0.cellId)
        }
    }
    
}

// MARK: - UICollectionViewDataSource

extension CollectionTableViewCell: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells[section].rowCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellItem = cells[indexPath.section]
        
        switch cellItem.type {
        case .video:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellItem.cellId, for: indexPath) as? VideoCollectionViewCell {
                guard let url = (cellItem as? VideoItem)?.videoUrl else { return cell }
                cell.videoUrl = url
                return cell
            }
        case .article:
             if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellItem.cellId, for: indexPath) as? ArticleCollectionViewCell {
                return cell
            }
        case .image:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellItem.cellId, for: indexPath) as? ImageCollectionViewCell {
                cell.imageName = (cellItem as? ImageItem)?.imageName
                return cell
            }
        }
        
        return UICollectionViewCell()
    }
    
}
