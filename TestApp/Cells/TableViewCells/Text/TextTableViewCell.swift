//
//  TextTableViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class TextTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
