//
//  HeaderTableViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 23/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    // MARK: - Outelts
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
