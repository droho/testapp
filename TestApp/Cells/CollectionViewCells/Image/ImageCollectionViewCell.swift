//
//  ImageCollectionViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 24/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets

    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: Public properties
    
    var imageName: String? {
        didSet {
            guard let imageName = imageName else { return }
            imageView.image = UIImage(named: imageName)
        }
    }
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addShadeView()
    }

}
