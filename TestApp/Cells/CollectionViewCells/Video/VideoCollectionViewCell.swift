//
//  VideoCollectionViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 24/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit
import AVFoundation

class VideoCollectionViewCell: UICollectionViewCell {

    // MARK: - Outelts
    
    @IBOutlet weak var playerView: PlayerView!
    
    // MARK: - Public properties
    
    var videoUrl: String? {
        didSet {
            startVideo()
        }
    }
    
    // MARK: - Private properties
    
    var player: AVPlayer?
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addShadeView()
    }
    
    override func prepareForReuse() {
        player = nil
    }
    
    deinit {
        removeObserver()
    }
    
    // MARK: - Configuration functions
    
    private func startVideo() {
        guard let url = videoUrl else { return }
        VideoManager.shared.getVideo(with: url) { result in
            switch result {
            case .success(let url):
                self.player = AVPlayer(url: url)
                self.playerView.player = self.player
                self.playerView.playerLayer.videoGravity = .resizeAspectFill
                self.playerView.player?.play()
                
                self.addOnFinishVideoObserver(item: self.player?.currentItem)
            case .failure(_): break
            }
        }
    }
    
    // MARK: - Actions
    
    @objc func playerDidFinishPlaying() {
        playerView.player?.seek(to: CMTime.zero)
        playerView.player?.play()
    }
    
    // MARK: - Observers
    
    private func addOnFinishVideoObserver(item: AVPlayerItem?) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: item)
    }
    
    private func removeObserver() {
         NotificationCenter.default.removeObserver(
            self,
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: nil)
    }

}
