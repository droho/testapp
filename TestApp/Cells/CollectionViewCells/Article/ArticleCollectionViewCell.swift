//
//  TextCollectionViewCell.swift
//  TestApp
//
//  Created by Damian Drohobycki on 24/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import UIKit

class ArticleCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.addShadeView()
    }

}
