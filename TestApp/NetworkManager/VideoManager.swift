//
//  CachedManager.swift
//  TestApp
//
//  Created by Damian Drohobycki on 28/11/2018.
//  Copyright © 2018 Damian Drohobycki. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(NSError)
}

class VideoManager {
    
    // MARK: - Static properties
    
    static let shared = VideoManager()
    
    // MARK: - Private properties
    
    private let fileManager = FileManager.default
    private lazy var mainDirectoryUrl: URL? = {
        guard let documentsUrl = self.fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first else { return nil }
        return documentsUrl
    }()
    
    // MARK: Public functions
    
    func getVideo(with stringUrl: String, completionHandler: @escaping (Result<URL>) -> Void ) {
        guard let file = directory(for: stringUrl) else { return }
        
        if fileManager.fileExists(atPath: file.path) {
            completionHandler(.success(file))
            return
        }
        
        DispatchQueue.global().async {
            guard let url = URL(string: stringUrl) else { return }
            if let videoData = NSData(contentsOf: url) {
                videoData.write(to: file, atomically: true)
                
                DispatchQueue.main.async {
                    completionHandler(.success(file))
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(.failure(NSError(domain: "Video download error", code: 1, userInfo: nil)))
                }
            }
        }
    }
    
    // MARK: - Private functions
    
    private func directory(for stringUrl: String) -> URL? {
        guard let url = URL(string: stringUrl) else { return nil }
        let fileURL = url.lastPathComponent
        let file = self.mainDirectoryUrl?.appendingPathComponent(fileURL)
        return file
    }
    
}
